using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repositories;
using DAL.Base.EF.Helpers;

namespace DAL.App.EF.Helpers
{
    public class AppRepositoryFactory : BaseRepositoryFactory<AppDbContext>
    {
        public AppRepositoryFactory()
        {
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            AddToCreationMethods<IProductRepository>(dataContext => new ProductRepository(dataContext));
            AddToCreationMethods<IOrderRepository>(dataContext => new OrderRepository(dataContext));
            AddToCreationMethods<IOrderRowRepository>(dataContext => new OrderRowRepository(dataContext));
            AddToCreationMethods<IProductInOrderRowRepository>(dataContext => new ProductInOrderRowRepository(dataContext));
            AddToCreationMethods<IAppUserRepository>(dataContext => new AppUserRepository(dataContext));
        }
        
    }
}