using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Helpers;
using DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : BaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext dataContext, IRepositoryProvider repositoryProvider) : base(dataContext,
            repositoryProvider)
        {
        }
        public IProductRepository ProductRepository => _repositoryProvider.GetRepository<IProductRepository>();
        public IAppUserRepository AppUserRepository => _repositoryProvider.GetRepository<IAppUserRepository>();
        public IOrderRepository OrderRepository => _repositoryProvider.GetRepository<IOrderRepository>();
        public IOrderRowRepository OrderRowRepository => _repositoryProvider.GetRepository<IOrderRowRepository>();
        public IProductInOrderRowRepository ProductInOrderRowRepository => _repositoryProvider.GetRepository<IProductInOrderRowRepository>();
    }
}