using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ProductRepository : BaseRepository<Product, Product, AppDbContext>, IProductRepository
    {
        public ProductRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext,
            new IdentityFunctionMapper())
        {
        }

        public async Task<IEnumerable<ProductDTO>> ProductsPrices(ProductType productType)
        {
            return await RepositoryDbSet.Where(p => p.ProductType == productType).Select(p => new ProductDTO()
            {
                Name = p.Name,
                Price = p.Price
            }).ToListAsync();
        }

        public async Task<IEnumerable<Product>> MainProducts()
        {
            return await RepositoryDbSet
                .Where(p => p.ProductType == ProductType.Pizza || p.ProductType == ProductType.Product)
                .ToListAsync();
        }

        public async Task<IEnumerable<ProductIdDTO>> ProductsByType(ProductType productType)
        {
            return await RepositoryDbSet
                .Where(p => p.ProductType == productType)
                .Select(p => new ProductIdDTO()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                    ProductType = p.ProductType
                }).ToListAsync();
        }

        public async Task<IEnumerable<ProductIdDTO>> ProductsNotInType(ProductType productType)
        {
            return await RepositoryDbSet
                .Where(p => p.ProductType != productType)
                .Select(p => new ProductIdDTO()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                    ProductType = p.ProductType
                }).OrderBy(p => p.ProductType)
                .ToListAsync();
        }
    }
}