using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class OrderRepository : BaseRepository<Order, Order, AppDbContext>, IOrderRepository
    {
        public OrderRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new IdentityFunctionMapper())
        {
        }

        public override Task<Order> FindAsync(params object[] id)
        {
            return RepositoryDbSet
                .Include(o => o.Rows)
                .ThenInclude(o => o.Product)
                .FirstOrDefaultAsync(o => o.Id == (string) id[0]);
        }
        public async Task<IEnumerable<Order>> AllAsync(string id)
        {
            return await RepositoryDbSet.Where(o => o.UserId == id).ToListAsync();
        }
        
        

        public async Task<OrderDTO> FindOrder(string id)
        {
            return await RepositoryDbSet
                .Include(o => o.Rows)
                .ThenInclude(o => o.Product)
                .Include(o => o.Rows)
                .ThenInclude(o => o.ExtraToppings)
                .ThenInclude(o => o.Product)
                .Include(o => o.Delivery)
                .Select(o => new OrderDTO()
                {
                    Id = o.Id,
                    Price = o.Price,
                    Comment = o.Comment,
                    OrderStatus = o.OrderStatus,
                    Delivery = new ProductIdDTO(){Id = o.Delivery.Id, Name = o.Delivery.Name, Price = o.Delivery.Price},
                    OrderRows = o.Rows.Select(r => new RowIdDTO()
                    {
                        Amount = r.Amount,
                        Id = r.Id,
                        Price = r.Price,
                        
                        Toppings = r.ExtraToppings.Select(t => new ProductIdDTO()
                        {
                            Id = t.Id,
                            Name = t.Product.Name,
                            Price = t.Product.Price
                        }),
                        Product = new ProductIdDTO()
                        {
                            Id = r.ProductId,
                            Name = r.Product.Name,
                            Price = r.Product.Price,
                            ProductType = r.Product.ProductType
                        }
                    }).ToList()
                })
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public async Task<IEnumerable<Order>> AllNotFinished()
        {
            return await RepositoryDbSet.Where(o => o.OrderStatus != OrderStatus.Done).ToListAsync();
        }

        public async Task<IEnumerable<OrderApiGet>> AllOrders(string id)
        {
            return await RepositoryDbSet
                .Where(o => o.UserId == id)
                .Include(o => o.Rows)
                .ThenInclude(o => o.Product)
                .Include(o => o.Rows)
                .ThenInclude(o => o.ExtraToppings)
                .ThenInclude(o => o.Product)
                .Include(o => o.Delivery)
                .Select(o => new OrderApiGet()
                {
                    Id = o.Id,
                    Price = o.Price,
                    DeliveryType = o.Delivery.Name,
                    OrderStatus = o.OrderStatus,
                    Comment = o.Comment,
                    OrderRows = o.Rows.Select(r => new OrderRowApiGet()
                    {
                        ProductName = r.Product.Name,
                        Amount = r.Amount,
                        Price = r.Price,
                        ExtraToppings = r.ExtraToppings.Select(et => new ProductApiGet()
                        {
                            Name = et.Product.Name,
                            Amount = r.ExtraToppings.Count(t => t.ProductId == et.ProductId),
                            Price = et.Product.Price * r.ExtraToppings.Count(t => t.ProductId == et.ProductId)
                        }).Distinct()
                    })
                }).ToListAsync();
        }
        
        public async Task<IEnumerable<OrderApiGet>> AllOrders()
        {
            return await RepositoryDbSet
                .Include(o => o.Rows)
                .ThenInclude(o => o.Product)
                .Include(o => o.Rows)
                .ThenInclude(o => o.ExtraToppings)
                .ThenInclude(o => o.Product)
                .Include(o => o.Delivery)
                .Select(o => new OrderApiGet()
                {
                    Id = o.Id,
                    Price = o.Price,
                    DeliveryType = o.Delivery.Name,
                    OrderStatus = o.OrderStatus,
                    Comment = o.Comment,
                    OrderRows = o.Rows.Select(r => new OrderRowApiGet()
                    {
                        ProductName = r.Product.Name,
                        Amount = r.Amount,
                        Price = r.Price,
                        ExtraToppings = r.ExtraToppings.Select(et => new ProductApiGet()
                        {
                            Name = et.Product.Name,
                            Amount = r.ExtraToppings.Count(t => t.ProductId == et.ProductId),
                            Price = et.Product.Price * r.ExtraToppings.Count(t => t.ProductId == et.ProductId)
                        }).Distinct()
                    })
                }).ToListAsync();
        }
    }
}