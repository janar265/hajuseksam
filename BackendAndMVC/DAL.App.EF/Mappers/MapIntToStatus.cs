using Domain;

namespace DAL.App.EF.Mappers
{
    public static class MapIntToStatus
    {
        public static OrderStatus Map(int i)
        {
            switch (i)
            {
                case 0:
                    return OrderStatus.Waiting;
                case 1:
                    return OrderStatus.Working;
                case 2:
                    return OrderStatus.Paid;
                case 3:
                    return OrderStatus.Done;
                default: return OrderStatus.Waiting;
            }
        }
    }
}