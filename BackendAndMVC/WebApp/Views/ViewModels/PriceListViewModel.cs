using System.Collections;
using System.Collections.Generic;
using DAL.App.EF.DTO;

namespace WebApp.Views.ViewModels
{
    public class PriceListViewModel
    {
        public IEnumerable<ProductDTO> PizzaPriceList { get; set; }
        public IEnumerable<ProductDTO> ToppingsPriceList { get; set; }
        public IEnumerable<ProductDTO> ProductsPriceList { get; set; }
    }
}