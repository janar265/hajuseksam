using System.Collections;
using System.Collections.Generic;
using DAL.App.EF.DTO;
using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Views.ViewModels
{
    public class EditOrderViewModel
    {
        public string EditableRowId { get; set; }
        public int ToppingsAmount { get; set; }
        public string ToppingId { get; set; }
        
        public SelectList Toppings { get; set; }
        public string ProductId { get; set; }
        public int ProductsAmount { get; set; }
        public SelectList Products { get; set; }
        public OrderDTO Order { get; set; }
        public ICollection<RowIdDTO> OrderRows { get; set; }
    }
}