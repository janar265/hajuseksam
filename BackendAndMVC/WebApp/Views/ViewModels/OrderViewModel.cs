using System.Collections;
using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Views.ViewModels
{
    public class OrderViewModel
    {
        public Product MainProduct { get; set; }
        public int Amount { get; set; }
        public string Comment { get; set; }
        public SelectList MainProducts { get; set; }
        public SelectList Deliveries { get; set; }
        public string DeliveryId { get; set; }
        public Product Delivery { get; set; }
    }
}