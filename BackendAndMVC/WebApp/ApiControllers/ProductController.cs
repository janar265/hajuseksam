using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.EF.DTO;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public ProductController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/Orders
        [Route("/api/product/deliveries")]
        [HttpGet]
        public async Task<IEnumerable<ProductIdDTO>> GetDeliveries()
        {
            return await _uow.ProductRepository.ProductsByType(ProductType.Delivery);
        }
    }
}