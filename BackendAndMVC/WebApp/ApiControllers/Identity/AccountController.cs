using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Domain.Identity;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApp.ApiControllers.Identity
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;

        public AccountController(IConfiguration configuration, SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager)
        {
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Login(LoginDTO model)
        {
            var appUser = await _userManager.FindByEmailAsync(model.Email);
            if (appUser == null)
            {
                return StatusCode(403);
            }

            var result = await _signInManager.PasswordSignInAsync(appUser, model.Password, false, false);

            if (!result.Succeeded) return StatusCode(403);
            var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
            var key = _configuration["JWT:key"];
            var issuer = _configuration["JWT:issuer"];
            var expiresIn = int.Parse(_configuration["JWT:ExpireDays"]);
            return Ok(new {token = JWTHelper.GenerateJwt(claimsPrincipal.Claims, key, issuer, expiresIn)});
        }

        [HttpPost]
        public async Task<ActionResult<string>> Register(LoginDTO model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                var resp = $"User {model.Email} is already registered";
                return NotFound(new {error = resp});
            }
            var appUser = new AppUser()
            {
                UserName = model.Email, Email = model.Email
            };
            var result = await _userManager.CreateAsync(appUser, model.Password);

            if (!result.Succeeded) return StatusCode(403);
            var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
            var key = _configuration["JWT:key"];
            var issuer = _configuration["JWT:issuer"];
            var expiresIn = int.Parse(_configuration["JWT:ExpireDays"]);
            return Ok(new {token = JWTHelper.GenerateJwt(claimsPrincipal.Claims, key, issuer, expiresIn)});
        }
        
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<string>> Auth()
        {
            var user = await _userManager.FindByIdAsync(User.GetUserId());
            var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(user);
            var key = _configuration["JWT:key"];
            var issuer = _configuration["JWT:issuer"];
            var expiresIn = int.Parse(_configuration["JWT:ExpireDays"]);
            return Ok(new {token = JWTHelper.GenerateJwt(claimsPrincipal.Claims, key, issuer, expiresIn)});
        }
        
        public class LoginDTO
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
    }
}