using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using DAL.App.EF.DTO;
using DAL.App.EF.Mappers;
using Domain;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrdersController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public OrdersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/Orders
//        [HttpGet]
//        public async Task<IEnumerable<Order>> GetOrders()
//        {
//            return await _uow.OrderRepository.AllAsync();
//        }
        
        [Route("/api/orders")]
        [HttpGet]
        public async Task<IEnumerable<OrderApiGet>> GetOrders()
        {
            return await _uow.OrderRepository.AllOrders(User.GetUserId());
        }
        
        [Route("/api/orders/admin")]
        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public async Task<IEnumerable<OrderApiGet>> GetAdminOrders()
        {
            return await _uow.OrderRepository.AllOrders();
        }
        
        [Route("/api/orders/status")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<ActionResult> ChangeStatus(StatusDTO statusDto)
        {
            var order = await _uow.OrderRepository.FindAsync(statusDto.Id);
            order.OrderStatus = MapIntToStatus.Map(statusDto.value);
            _uow.OrderRepository.Update(order);
            await _uow.SaveChangesAsync();
            return Ok(new {status = "Success"});
        }
        
        // POST: api/Orders
        [Route("/api/orders")]
        [HttpPost]
        public async Task<ActionResult<OrderDTO>> PostOrder([FromBody] OrderDTO order)
        {
            Console.WriteLine(order);
            var dbOrder = new Order();
            dbOrder.UserId = User.GetUserId();
            dbOrder.OrderStatus = OrderStatus.Waiting;
            dbOrder.Comment = order.Comment;
            dbOrder.DeliveryId = order.Delivery.Id;
            await _uow.OrderRepository.AddAsync(dbOrder);
            foreach (var row in order.OrderRows)
            {
                var orderRow = new OrderRow();
                orderRow.OrderId = dbOrder.Id;
                orderRow.ProductId = row.Id;
                orderRow.Amount = row.Amount;
                orderRow.Price = row.Price;
                dbOrder.Price += orderRow.Price;
                await _uow.OrderRowRepository.AddAsync(orderRow);
                if (row.Toppings == null) continue;
                foreach (var topping in row.Toppings)
                {
                    for (int i = 0; i < topping.Amount; i++)
                    {
                        var dbTopping = new ProductInOrderRow();
                        dbTopping.ProductId = topping.Id;
                        dbTopping.OrderRowId = orderRow.Id;
                        await _uow.ProductInOrderRowRepository.AddAsync(dbTopping);
                    }
                }
            }
            await _uow.SaveChangesAsync();
            
            return order;
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(string id)
        {
            var order = await _uow.OrderRepository.FindAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return order;
        }

//        // PUT: api/Orders/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutOrder(string id, Order order)
//        {
//            if (id != order.Id)
//            {
//                return BadRequest();
//            }
//
//            _context.Entry(order).State = EntityState.Modified;
//            
//
//            try
//            {
//                await _uow.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!OrderExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return NoContent();
//        }
//
//
//        // DELETE: api/Orders/5
//        [HttpDelete("{id}")]
//        public async Task<ActionResult<Order>> DeleteOrder(string id)
//        {
//            var order = await _context.Orders.FindAsync(id);
//            if (order == null)
//            {
//                return NotFound();
//            }
//
//            _context.Orders.Remove(order);
//            await _context.SaveChangesAsync();
//
//            return order;
//        }
//
//        private bool OrderExists(string id)
//        {
//            return _context.Orders.Any(e => e.Id == id);
//        }
    }
}
