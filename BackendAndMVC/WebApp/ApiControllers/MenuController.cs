using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App;
using DAL.App.EF.DTO;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public MenuController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<IEnumerable<ProductIdDTO>> GetMenu()
        {
            return await _uow.ProductRepository.ProductsNotInType(ProductType.Delivery);
        }
    }
}