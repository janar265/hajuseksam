using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class OrderRowController : Controller
    {
        private readonly AppDbContext _context;

        public OrderRowController(AppDbContext context)
        {
            _context = context;
        }

        // GET: OrderRow
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.OrderRows.Include(o => o.Product);
            return View(await appDbContext.ToListAsync());
        }

        // GET: OrderRow/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _context.OrderRows
                .Include(o => o.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderRow == null)
            {
                return NotFound();
            }

            return View(orderRow);
        }

        // GET: OrderRow/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, nameof(Product.Id), nameof(Product.Name));
            return View();
        }

        // POST: OrderRow/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,Amount,Price,Id,CreatedTs")] OrderRow orderRow)
        {
            if (ModelState.IsValid)
            {
                _context.Add(orderRow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, nameof(Product.Id), nameof(Product.Name), orderRow.ProductId);
            return View(orderRow);
        }

        // GET: OrderRow/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _context.OrderRows.FindAsync(id);
            if (orderRow == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, nameof(Product.Id), nameof(Product.Name), orderRow.ProductId);
            return View(orderRow);
        }

        // POST: OrderRow/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ProductId,Amount,Price,Id,CreatedTs")] OrderRow orderRow)
        {
            if (id != orderRow.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orderRow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderRowExists(orderRow.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, nameof(Product.Id), nameof(Product.Name), orderRow.ProductId);
            return View(orderRow);
        }

        // GET: OrderRow/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _context.OrderRows
                .Include(o => o.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderRow == null)
            {
                return NotFound();
            }

            return View(orderRow);
        }

        // POST: OrderRow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var orderRow = await _context.OrderRows.FindAsync(id);
            _context.OrderRows.Remove(orderRow);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderRowExists(string id)
        {
            return _context.OrderRows.Any(e => e.Id == id);
        }
    }
}
