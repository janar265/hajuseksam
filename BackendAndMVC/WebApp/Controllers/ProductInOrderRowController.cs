using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class ProductInOrderRowController : Controller
    {
        private readonly AppDbContext _context;

        public ProductInOrderRowController(AppDbContext context)
        {
            _context = context;
        }

        // GET: ProductInOrderRow
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.ProductInOrderRows.Include(p => p.OrderRow).Include(p => p.Product);
            return View(await appDbContext.ToListAsync());
        }

        // GET: ProductInOrderRow/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInOrderRow = await _context.ProductInOrderRows
                .Include(p => p.OrderRow)
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productInOrderRow == null)
            {
                return NotFound();
            }

            return View(productInOrderRow);
        }

        // GET: ProductInOrderRow/Create
        public IActionResult Create()
        {
            ViewData["OrderRowId"] = new SelectList(_context.OrderRows, "Id", "Id");
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id");
            return View();
        }

        // POST: ProductInOrderRow/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderRowId,ProductId,Id,CreatedTs")] ProductInOrderRow productInOrderRow)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productInOrderRow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["OrderRowId"] = new SelectList(_context.OrderRows, "Id", "Id", productInOrderRow.OrderRowId);
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productInOrderRow.ProductId);
            return View(productInOrderRow);
        }

        // GET: ProductInOrderRow/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInOrderRow = await _context.ProductInOrderRows.FindAsync(id);
            if (productInOrderRow == null)
            {
                return NotFound();
            }
            ViewData["OrderRowId"] = new SelectList(_context.OrderRows, "Id", "Id", productInOrderRow.OrderRowId);
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productInOrderRow.ProductId);
            return View(productInOrderRow);
        }

        // POST: ProductInOrderRow/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("OrderRowId,ProductId,Id,CreatedTs")] ProductInOrderRow productInOrderRow)
        {
            if (id != productInOrderRow.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productInOrderRow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductInOrderRowExists(productInOrderRow.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OrderRowId"] = new SelectList(_context.OrderRows, "Id", "Id", productInOrderRow.OrderRowId);
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productInOrderRow.ProductId);
            return View(productInOrderRow);
        }

        // GET: ProductInOrderRow/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInOrderRow = await _context.ProductInOrderRows
                .Include(p => p.OrderRow)
                .Include(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productInOrderRow == null)
            {
                return NotFound();
            }

            return View(productInOrderRow);
        }

        // POST: ProductInOrderRow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var productInOrderRow = await _context.ProductInOrderRows.FindAsync(id);
            _context.ProductInOrderRows.Remove(productInOrderRow);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductInOrderRowExists(string id)
        {
            return _context.ProductInOrderRows.Any(e => e.Id == id);
        }
    }
}
