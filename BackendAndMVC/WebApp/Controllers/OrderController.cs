using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Identity;
using Microsoft.AspNetCore.Authorization;
using WebApp.Views.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public OrderController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Order
        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("Administrator"))
            {
                return View(await _uow.OrderRepository.AllNotFinished());
            }
            return View(await _uow.OrderRepository.AllAsync(User.GetUserId()));
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.OrderRepository.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Order/Create
        public async Task<ViewResult> Create()
        {
            var vm = new OrderViewModel();
            vm.MainProducts = new SelectList(await _uow.ProductRepository.MainProducts(), nameof(Product.Id),
                nameof(Product.Name));
            vm.Deliveries = new SelectList(await _uow.ProductRepository.ProductsByType(ProductType.Delivery), nameof(Product.Id), nameof(Product.Name));
            return View(vm);
        }

        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OrderViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var product = await _uow.ProductRepository.FindAsync(vm.MainProduct.Id);
                var order = new Order();
                order.UserId = User.GetUserId();
                order.OrderStatus = OrderStatus.Waiting;
                order.Comment = vm.Comment;
                order.DeliveryId = vm.DeliveryId;
                await _uow.OrderRepository.AddAsync(order);
                var orderRow = new OrderRow();
                orderRow.OrderId = order.Id;
                orderRow.ProductId = vm.MainProduct.Id;
                orderRow.Amount = vm.Amount;
                orderRow.Price = product.Price;
                order.Price = orderRow.Price * orderRow.Amount;
                await _uow.OrderRowRepository.AddAsync(orderRow);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Edit), new { id = order.Id });
            }
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ChangeStatus(string orderId, OrderStatus orderStatus)
        {
            if (orderId == null)
            {
                return NotFound();
            }

            var order = await _uow.OrderRepository.FindAsync(orderId);
            order.OrderStatus = orderStatus;
            _uow.OrderRepository.Update(order);
            await _uow.SaveChangesAsync();
            
            return RedirectToAction(nameof(Index));
        }
        
//         GET: Order/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.OrderRepository.FindOrder(id);
            if (order == null)
            {
                return NotFound();
            }
            var vm = new EditOrderViewModel();
            vm.Order = order;
            vm.OrderRows = order.OrderRows;
            return View(vm);
        }
        
        public async Task<IActionResult> AddToppings(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _uow.OrderRowRepository.FindAsync(id);
            var order = await _uow.OrderRepository.FindOrder(orderRow.OrderId);
            if (order == null)
            {
                return NotFound();
            }
            var vm = new EditOrderViewModel();
            vm.Order = order;
            vm.OrderRows = order.OrderRows;
            vm.EditableRowId = id;
            vm.Toppings = new SelectList(await _uow.ProductRepository.ProductsByType(ProductType.Topping), nameof(Product.Id), nameof(Product.Name));
            return View(vm);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddToppings(EditOrderViewModel vm)
        {
            if (vm.ToppingsAmount > 0)
            {
                var order = await _uow.OrderRepository.FindAsync(vm.Order.Id);
                var product = await _uow.ProductRepository.FindAsync(vm.ToppingId);
                for (int j = 0; j < vm.ToppingsAmount; j++)
                {
                    var topping = new ProductInOrderRow();
                    topping.ProductId = vm.ToppingId;
                    topping.OrderRowId = vm.EditableRowId;
                    await _uow.ProductInOrderRowRepository.AddAsync(topping);
                }
                order.Price += product.Price * vm.ToppingsAmount;
                _uow.OrderRepository.Update(order);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Edit), new { id = vm.Order.Id });
            }
            return View(vm);
        }
        
        public async Task<IActionResult> AddItems(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.OrderRepository.FindOrder(id);
            if (order == null)
            {
                return NotFound();
            }
            var vm = new EditOrderViewModel();
            vm.Order = order;
            vm.OrderRows = order.OrderRows;
            vm.Products = new SelectList(await _uow.ProductRepository.MainProducts(), nameof(Product.Id), nameof(Product.Name));
            return View(vm);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItems(EditOrderViewModel vm)
        {
            if (vm.ProductsAmount > 0)
            {
                var order = await _uow.OrderRepository.FindAsync(vm.Order.Id);
                var product = await _uow.ProductRepository.FindAsync(vm.ProductId);
                var orderRow = new OrderRow();
                orderRow.ProductId = vm.ProductId;
                orderRow.Amount = vm.ProductsAmount;
                orderRow.OrderId = order.Id;
                orderRow.Price = product.Price * vm.ProductsAmount;
                await _uow.OrderRowRepository.AddAsync(orderRow);
                order.Price += product.Price * vm.ProductsAmount;
                _uow.OrderRepository.Update(order);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Edit), new { id = vm.Order.Id });
            }
            return View(vm);
        }
//
//        // POST: Order/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> Edit(string id, [Bind("Price,OrderStatus,Comment,Id,CreatedTs")] Order order)
//        {
//            if (id != order.Id)
//            {
//                return NotFound();
//            }
//
//            if (ModelState.IsValid)
//            {
//                try
//                {
//                    _context.Update(order);
//                    await _context.SaveChangesAsync();
//                }
//                catch (DbUpdateConcurrencyException)
//                {
//                    if (!OrderExists(order.Id))
//                    {
//                        return NotFound();
//                    }
//                    else
//                    {
//                        throw;
//                    }
//                }
//                return RedirectToAction(nameof(Index));
//            }
//            return View(order);
//        }
//
        // GET: Order/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.OrderRepository.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var order = await _uow.OrderRepository.FindAsync(id);
            _uow.OrderRepository.Remove(order);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
//
//        private bool OrderExists(string id)
//        {
//            return _context.Orders.Any(e => e.Id == id);
//        }
    }
}