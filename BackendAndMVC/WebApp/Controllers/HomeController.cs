﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Domain;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Views.ViewModels;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {

        private readonly IAppUnitOfWork _uow;

        public HomeController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        public async Task<ViewResult> Index()
        {
            var vm = new PriceListViewModel();
            vm.PizzaPriceList = await _uow.ProductRepository.ProductsPrices(ProductType.Pizza);
            vm.ProductsPriceList = await _uow.ProductRepository.ProductsPrices(ProductType.Product);
            vm.ToppingsPriceList = await _uow.ProductRepository.ProductsPrices(ProductType.Topping);
            return View(vm);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}