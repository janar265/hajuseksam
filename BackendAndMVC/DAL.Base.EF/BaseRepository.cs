﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF
{
    public class BaseRepository<TDALEntity, TDomainEntity, TDbContext> : IBaseRepository<TDALEntity>
        where TDomainEntity : class, new()
        where TDbContext : DbContext
        where TDALEntity : class, new()
    {
        protected readonly DbContext RepositoryDbContext;
        protected readonly DbSet<TDomainEntity> RepositoryDbSet;
        private readonly IBaseDALMapper _mapper;

        protected readonly IDictionary<object, TDomainEntity> EntityCreationCache =
            new Dictionary<object, TDomainEntity>();

        public BaseRepository(TDbContext repositoryDbContext, IBaseDALMapper mapper)
        {
            RepositoryDbContext = repositoryDbContext;
            RepositoryDbSet = RepositoryDbContext.Set<TDomainEntity>();
            _mapper = mapper;
        }

        public virtual TDALEntity Update(TDALEntity entity)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Update(_mapper.Map<TDomainEntity>(entity)).Entity);
        }

        public virtual void Remove(TDALEntity entity)
        {
            RepositoryDbSet.Remove(_mapper.Map<TDomainEntity>(entity));
        }

        public virtual void Remove(params object[] id)
        {
            RepositoryDbSet.Remove(RepositoryDbSet.Find(id));
        }

        public TDALEntity GetUpdatesAfterUowSaveChanges(TDALEntity entity)
        {
            var domainEntity = _mapper.Map<TDomainEntity>(entity);
            return EntityCreationCache.ContainsKey(domainEntity.Id)
                ? _mapper.Map<TDALEntity>(EntityCreationCache[domainEntity.Id])
                : entity;
        }

        public virtual async Task<IEnumerable<TDALEntity>> AllAsync()
        {
            return (await RepositoryDbSet.ToListAsync())
                .Select(e => _mapper.Map<TDALEntity>(e)).ToList();
        }

        public virtual async Task<TDALEntity> FindAsync(params object[] id)
        {
            return _mapper.Map<TDALEntity>((await RepositoryDbSet.FindAsync(id)));
        }

        public virtual async Task<TDALEntity> AddAsync(TDALEntity entity)
        {
            //EntityCreationCache
            var res = (await RepositoryDbSet.AddAsync(_mapper.Map<TDomainEntity>(entity))).Entity;
            EntityCreationCache.Add(res.Id, res);
            return _mapper.Map<TDALEntity>(res);
        }

        public IEnumerable<TDALEntity> All()
        {
            return RepositoryDbSet.Select(e => _mapper.Map<TDALEntity>(e)).ToList();
        }

        public TDALEntity Find(params object[] id)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Find(id));
        }

        public TDALEntity Add(TDALEntity entity)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Add(_mapper.Map<TDomainEntity>(entity)).Entity);
        }
    }
}