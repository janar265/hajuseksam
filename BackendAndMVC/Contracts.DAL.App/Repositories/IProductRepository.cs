using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.EF.DTO;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<IEnumerable<ProductDTO>> ProductsPrices(ProductType productType);
        Task<IEnumerable<Product>> MainProducts();
        Task<IEnumerable<ProductIdDTO>> ProductsByType(ProductType productType);
        Task<IEnumerable<ProductIdDTO>> ProductsNotInType(ProductType productType);
    }
}