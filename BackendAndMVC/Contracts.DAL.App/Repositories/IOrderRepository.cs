using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.EF.DTO;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<IEnumerable<Order>> AllAsync(string id);
        Task<OrderDTO> FindOrder(string id);
        Task<IEnumerable<Order>> AllNotFinished();
        Task<IEnumerable<OrderApiGet>> AllOrders(string id);
        Task<IEnumerable<OrderApiGet>> AllOrders();
        
    }
}