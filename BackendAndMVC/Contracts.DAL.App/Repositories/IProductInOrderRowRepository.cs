using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductInOrderRowRepository : IBaseRepository<ProductInOrderRow>
    {
        
    }
}