using Contracts.DAL.Base.Repositories;
using Domain.Identity;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserRepository : IBaseRepository<AppUser>
    {
        
    }
}