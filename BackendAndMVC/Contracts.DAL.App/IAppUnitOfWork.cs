﻿using System;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IProductRepository ProductRepository { get; }
        IAppUserRepository AppUserRepository { get; }
        IOrderRepository OrderRepository { get; }
        IOrderRowRepository OrderRowRepository{ get; }
        IProductInOrderRowRepository ProductInOrderRowRepository { get; }
    }
}