namespace Domain
{
    public class ProductInOrderRow : BaseEntity
    {
        public string OrderRowId { get; set; }
        public OrderRow OrderRow { get; set; }

        public string ProductId { get; set; }
        public Product Product { get; set; }
    }
}