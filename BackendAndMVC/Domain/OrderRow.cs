using System.Collections.Generic;

namespace Domain
{
    public class OrderRow : BaseEntity
    {
        public string ProductId { get; set; }
        public Product Product { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }

        public string OrderId { get; set; }
        public Order Order { get; set; }
        
        public ICollection<ProductInOrderRow> ExtraToppings { get; set; }
    }
}