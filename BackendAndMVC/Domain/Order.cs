using System.Collections.Generic;
using Domain.Identity;

namespace Domain
{
    public class Order : BaseEntity
    {
        public string UserId { get; set; }
        public AppUser User { get; set; }
        public decimal Price { get; set; }
        public string DeliveryId { get; set; }
        public Product Delivery { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public string Comment { get; set; }

        public ICollection<OrderRow> Rows { get; set; }
    }
}