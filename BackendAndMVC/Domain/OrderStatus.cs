namespace Domain
{
    public enum OrderStatus
    {
        Waiting,
        Paid,
        Working,
        Done
    }
}