namespace Domain
{
    public enum ProductType
    {
        Pizza,
        Product,
        Topping,
        Delivery
    }
}