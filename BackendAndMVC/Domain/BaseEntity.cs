using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.DAL.Base;

namespace Domain
{
    public abstract class BaseEntity : IDomainEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(40)]
        [MinLength(1)]
        [Required]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public DateTime CreatedTs { get; set; } = DateTime.Now;
    }
}