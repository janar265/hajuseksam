using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.DAL.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Identity
{
    public class AppUser : IdentityUser<string>, IDomainEntity
    {
        public DateTime CreatedTs { get; set; } = DateTime.Now;
        
        [InverseProperty("User")]
        public ICollection<Order> Orders { get; set; }
    }
}