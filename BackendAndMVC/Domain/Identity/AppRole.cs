using System;
using Contracts.DAL.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Identity
{
    public class AppRole : IdentityRole<string>, IDomainEntity
    {
        public DateTime CreatedTs { get; set; } = DateTime.Now;
    }
}