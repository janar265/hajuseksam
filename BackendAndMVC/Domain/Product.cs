using System.Collections.Generic;

namespace Domain
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public ProductType ProductType { get; set; }

        public ICollection<ProductInOrderRow> OrderRows { get; set; }
    }
}