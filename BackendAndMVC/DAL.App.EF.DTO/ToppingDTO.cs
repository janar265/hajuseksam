namespace DAL.App.EF.DTO
{
    public class ToppingDTO
    {
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}