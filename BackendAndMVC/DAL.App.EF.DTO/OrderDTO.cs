using System.Collections.Generic;
using Domain;

namespace DAL.App.EF.DTO
{
    public class OrderDTO
    {
        public string Id { get; set; }
        public ICollection<RowIdDTO> OrderRows { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public ProductIdDTO Delivery { get; set; }
        public decimal Price { get; set; }
        public string Comment { get; set; }
    }
}