using System.Collections;
using System.Collections.Generic;

namespace DAL.App.EF.DTO
{
    public class RowIdDTO
    {
        public string Id { get; set; }
        public ProductIdDTO Product { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public IEnumerable<ProductIdDTO> Toppings { get; set; }
    }
}