﻿using System;
using Domain;

namespace DAL.App.EF.DTO
{
    public class ProductDTO
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public ProductType ProductType { get; set; }
    }
}