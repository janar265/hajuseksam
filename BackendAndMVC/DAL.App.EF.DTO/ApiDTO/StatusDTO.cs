using Domain;

namespace DAL.App.EF.DTO
{
    public class StatusDTO
    {
        public string Id { get; set; }
        public int value { get; set; }
    }
}