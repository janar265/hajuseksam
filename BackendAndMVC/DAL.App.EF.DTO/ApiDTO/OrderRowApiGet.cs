using System.Collections.Generic;

namespace DAL.App.EF.DTO
{
    public class OrderRowApiGet
    {
        public string ProductName { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public IEnumerable<ProductApiGet> ExtraToppings { get; set; }
    }
}