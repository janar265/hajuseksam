using System.Collections.Generic;
using Domain;

namespace DAL.App.EF.DTO
{
    public class OrderApiGet
    {
        public string Id { get; set; }
        public string Comment { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public decimal Price { get; set; }
        public string DeliveryType { get; set; }
        public IEnumerable<OrderRowApiGet> OrderRows { get; set; }
    }
}