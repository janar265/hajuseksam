namespace DAL.App.EF.DTO
{
    public class ProductApiGet
    {
        public string Name { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
    }
}