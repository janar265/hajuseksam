using Domain;

namespace DAL.App.EF.DTO
{
    public class ProductIdDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        
        public int Amount { get; set; }
        public ProductType ProductType { get; set; }
    }
}